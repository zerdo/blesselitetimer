﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlessEliteTimer
{
    public partial class BlessEliteTimer : MetroForm
    {
        bool _key1Pressed = false;
        bool _key2Pressed = false;

        Keys _channel1Keybind;
        Keys _channel2Keybind;

        Overlay _overlay;

        public BlessEliteTimer()
        {
            InitializeComponent();
            _overlay = new Overlay();
            Enum.TryParse(Properties.Settings.Default.keybindChannel1,out _channel1Keybind);
            Enum.TryParse(Properties.Settings.Default.keybindChannel2, out _channel2Keybind);

            //Set default keys if keybinds aren't set
            if(_channel1Keybind == Keys.None)
            {
                _channel1Keybind = Keys.Up;
                Properties.Settings.Default.keybindChannel1 = _channel1Keybind.ToString();
                Properties.Settings.Default.Save();
            }
            if (_channel2Keybind == Keys.None)
            {
                _channel2Keybind = Keys.Down;
                Properties.Settings.Default.keybindChannel2 = _channel2Keybind.ToString();
                Properties.Settings.Default.Save();
            }
        }

        [DllImport("user32.dll")]
        static extern short GetAsyncKeyState(System.Windows.Forms.Keys vKey);

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        public static bool IsKeyPushedDown(System.Windows.Forms.Keys vKey)
        {
            return 0 != (GetAsyncKeyState(vKey) & 0x8000);
        }
        public static bool IsKeyUp(System.Windows.Forms.Keys vKey)
        {
            return GetAsyncKeyState(vKey) == 0;
        }

        private void timerInput_Tick(object sender, EventArgs e)
        {           
            if(IsKeyPushedDown(_channel1Keybind) && !_key1Pressed)
            {
                _key1Pressed = true;
                _overlay.runningChannel1 = !_overlay.runningChannel1;
                if (_overlay.runningChannel1)
                {
                    _overlay.SetTime(1);
                }                
            }
            else if(IsKeyUp(_channel1Keybind))
            {               
                _key1Pressed = false;                
            }
            if (IsKeyPushedDown(_channel2Keybind) && !_key2Pressed)
            {
                _key2Pressed = true;
                _overlay.runningChannel2 = !_overlay.runningChannel2;
                if (_overlay.runningChannel2)
                {
                    _overlay.SetTime(2);
                }                
            }
            else if (IsKeyUp(_channel2Keybind))
            {
                _key2Pressed = false;                
            }
        }

        private void btnMin_Click(object sender, EventArgs e)
        {
            this.Hide();
            notifyIcon1.Visible = true;
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            if(settings.ShowDialog() == DialogResult.OK)
            {
                Enum.TryParse(Properties.Settings.Default.keybindChannel1, out _channel1Keybind);
                Enum.TryParse(Properties.Settings.Default.keybindChannel2, out _channel2Keybind);
            }
        }

        private void mlReddit_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.reddit.com/r/Bless/comments/8pdcxv/tool_bless_elite_timer/");
        }

        private void mlDiscord_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://discord.gg/RTp9smU");
        }

        private void mlCyph_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.cyph.gg/blesselitetimer");
        }
    }
}

