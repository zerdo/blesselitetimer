﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlessEliteTimer
{
    public partial class Overlay : Form
    {
        public enum GWL
        {
            ExStyle = -20
        }

        public enum WS_EX
        {
            Transparent = 0x20,
            Layered = 0x80000
        }

        public enum LWA
        {
            ColorKey = 0x1,
            Alpha = 0x2
        }
        private const int WM_MOUSEACTIVATE = 0x0021;
        private const int MA_NOACTIVATEANDEAT = 0x0004;
        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

        [DllImport("user32.dll")]
        static extern bool EnableWindow(IntPtr hWnd, bool bEnable);

        [DllImport("user32.dll", EntryPoint = "GetWindowLong")]
        public static extern int GetWindowLong(IntPtr hWnd, GWL nIndex);

        [DllImport("user32.dll", EntryPoint = "SetWindowLong")]
        public static extern int SetWindowLong(IntPtr hWnd, GWL nIndex, int dwNewLong);

        [DllImport("user32.dll", EntryPoint = "SetLayeredWindowAttributes")]
        public static extern bool SetLayeredWindowAttributes(IntPtr hWnd, int crKey, byte alpha, LWA dwFlags);


        DateTime startTimeChannel1;
        DateTime nextSpawnTimeChannel1;

        DateTime startTimeChannel2;
        DateTime nextSpawnTimeChannel2;

        public bool runningChannel1 { get; set; }
        public bool runningChannel2 { get; set; }

        public bool IS_RUNNING { get; set; }
        public Overlay()
        {
            InitializeComponent();
        }

        public void SetTime(int channel)
        {
            switch (channel)
            {
                case 1:
                    startTimeChannel1 = DateTime.Now;
                    nextSpawnTimeChannel1 = startTimeChannel1.AddSeconds(90);
                    runningChannel1 = true;
                    break;
                case 2:
                    startTimeChannel2 = DateTime.Now;
                    nextSpawnTimeChannel2 = startTimeChannel2.AddSeconds(90);
                    runningChannel2 = true;
                    break;
            }
        }

        protected override bool ShowWithoutActivation
        {
            get { return true; }
        }
        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            int wl = GetWindowLong(this.Handle, GWL.ExStyle);
            wl = wl | 0x80000 | 0x20;
            SetWindowLong(this.Handle, GWL.ExStyle, wl);
            //SetLayeredWindowAttributes(this.Handle, 0, 128, LWA.Alpha);
            this.Location = new Point(300, 200);

        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x80;
                return cp;
            }
        }

        private void Overlay_Load(object sender, EventArgs e)
        {
            SetWindowPos(this.Handle, -1, 0, 0, 0, 0, 0x0001 | 0x0002 | 0x0010 | 0x08000000);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            var secondsChannel1 = 0;
            var secondsChannel2 = 0;
            if (runningChannel1)
            {
                secondsChannel1 = Convert.ToInt16((nextSpawnTimeChannel1 - DateTime.Now).TotalSeconds);
                if (secondsChannel1 <= 0)
                {
                    secondsChannel1 = 0;
                    runningChannel1 = false;
                }
            }
            if(runningChannel2)
            {
                secondsChannel2 = Convert.ToInt16((nextSpawnTimeChannel2 - DateTime.Now).TotalSeconds);
                if (secondsChannel2 <= 0)
                {
                    secondsChannel2 = 0;
                    runningChannel2 = false;
                }
            }

            lbTimeleft.Text = secondsChannel1.ToString();
            lbTimeleft2.Text = secondsChannel2.ToString();

            if (!runningChannel1 && !runningChannel2)
            {
                if (this.Visible)
                    this.Hide();
            }
            else if (!this.Visible)
            {
                this.Show();
            }
        }
    }
}
