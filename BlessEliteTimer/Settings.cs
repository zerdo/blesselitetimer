﻿using MetroFramework.Forms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlessEliteTimer
{
    public partial class Settings : MetroForm
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void Settings_Load(object sender, EventArgs e)
        {            
            ArrayList KeysList = new ArrayList();
            Type keyType = typeof(System.Windows.Forms.Keys);
            MemberInfo[] propInfoList2 = keyType.GetMembers(BindingFlags.Static |
                                          BindingFlags.DeclaredOnly | BindingFlags.Public);
            foreach (MemberInfo a in propInfoList2)
            {
                if (a.MemberType != MemberTypes.Method)
                {
                    this.cmbKey1.Items.Add(a.Name);
                    this.cmbKey2.Items.Add(a.Name);
                }
            }

            cmbKey1.SelectedItem = Properties.Settings.Default.keybindChannel1;
            cmbKey2.SelectedItem = Properties.Settings.Default.keybindChannel2;
        }        

        private void btnSave_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.keybindChannel2 = cmbKey2.SelectedItem.ToString();
            Properties.Settings.Default.keybindChannel1 = cmbKey1.SelectedItem.ToString();
            Properties.Settings.Default.Save();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
