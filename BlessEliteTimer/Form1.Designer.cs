﻿namespace BlessEliteTimer
{
    partial class BlessEliteTimer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlessEliteTimer));
            this.timerInput = new System.Windows.Forms.Timer(this.components);
            this.btnMin = new MetroFramework.Controls.MetroButton();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.btnSettings = new MetroFramework.Controls.MetroButton();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.mlReddit = new MetroFramework.Controls.MetroLink();
            this.mlDiscord = new MetroFramework.Controls.MetroLink();
            this.mlCyph = new MetroFramework.Controls.MetroLink();
            this.SuspendLayout();
            // 
            // timerInput
            // 
            this.timerInput.Enabled = true;
            this.timerInput.Interval = 10;
            this.timerInput.Tick += new System.EventHandler(this.timerInput_Tick);
            // 
            // btnMin
            // 
            this.btnMin.Location = new System.Drawing.Point(65, 68);
            this.btnMin.Name = "btnMin";
            this.btnMin.Size = new System.Drawing.Size(134, 23);
            this.btnMin.Style = MetroFramework.MetroColorStyle.Lime;
            this.btnMin.TabIndex = 8;
            this.btnMin.TabStop = false;
            this.btnMin.Text = "Hide";
            this.btnMin.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnMin.UseSelectable = true;
            this.btnMin.UseStyleColors = true;
            this.btnMin.Click += new System.EventHandler(this.btnMin_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Bless Elite Timer";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // btnSettings
            // 
            this.btnSettings.Location = new System.Drawing.Point(65, 39);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(134, 23);
            this.btnSettings.Style = MetroFramework.MetroColorStyle.Lime;
            this.btnSettings.TabIndex = 9;
            this.btnSettings.TabStop = false;
            this.btnSettings.Text = "Settings";
            this.btnSettings.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnSettings.UseSelectable = true;
            this.btnSettings.UseStyleColors = true;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(80, 11);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(103, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel2.TabIndex = 10;
            this.metroLabel2.Text = "Bless Elite Timer";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabel2.UseStyleColors = true;
            // 
            // mlReddit
            // 
            this.mlReddit.Location = new System.Drawing.Point(52, 97);
            this.mlReddit.Name = "mlReddit";
            this.mlReddit.Size = new System.Drawing.Size(46, 23);
            this.mlReddit.TabIndex = 11;
            this.mlReddit.Text = "Reddit";
            this.mlReddit.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mlReddit.UseSelectable = true;
            this.mlReddit.Click += new System.EventHandler(this.mlReddit_Click);
            // 
            // mlDiscord
            // 
            this.mlDiscord.Location = new System.Drawing.Point(104, 97);
            this.mlDiscord.Name = "mlDiscord";
            this.mlDiscord.Size = new System.Drawing.Size(50, 23);
            this.mlDiscord.TabIndex = 12;
            this.mlDiscord.Text = "Discord";
            this.mlDiscord.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mlDiscord.UseSelectable = true;
            this.mlDiscord.Click += new System.EventHandler(this.mlDiscord_Click);
            // 
            // mlCyph
            // 
            this.mlCyph.Location = new System.Drawing.Point(160, 97);
            this.mlCyph.Name = "mlCyph";
            this.mlCyph.Size = new System.Drawing.Size(50, 23);
            this.mlCyph.TabIndex = 13;
            this.mlCyph.Text = "cyph.gg";
            this.mlCyph.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mlCyph.UseSelectable = true;
            this.mlCyph.Click += new System.EventHandler(this.mlCyph_Click);
            // 
            // BlessEliteTimer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(263, 120);
            this.Controls.Add(this.mlCyph);
            this.Controls.Add(this.mlDiscord);
            this.Controls.Add(this.mlReddit);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.btnMin);
            this.DisplayHeader = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BlessEliteTimer";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Lime;
            this.Text = "Bless Elite Timer";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timerInput;
        private MetroFramework.Controls.MetroButton btnMin;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private MetroFramework.Controls.MetroButton btnSettings;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLink mlReddit;
        private MetroFramework.Controls.MetroLink mlDiscord;
        private MetroFramework.Controls.MetroLink mlCyph;
    }
}

